package com.example.springboot.model;

import org.springframework.data.annotation.*;

import java.util.Date;

public class User {
    @Id
    public String id;

    public String firstName;
    public String lastName;

    @CreatedBy
    private String createUser;

    @CreatedDate
    private Date createdDate;

    @LastModifiedBy
    private String lastModifiedUser;

    @LastModifiedDate
    private Date updateDttm;

    public User() {}

    public User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", createUser="
                + createUser + ", createdDate=" + createdDate + ", lastModifiedUser=" + lastModifiedUser + "]";
    }
}
