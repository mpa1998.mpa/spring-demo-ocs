package com.example.springboot.controller;

import com.example.springboot.config.handler.exception.NotFoundException;
import com.example.springboot.model.User;
import com.example.springboot.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
public class UserController {

	@Autowired
	private UserRepository repository;

	@RequestMapping(path = "/", method = RequestMethod.GET)
	public String index() {
		return "Greetings from Spring Boot 1!";
	}

	@RequestMapping(method = RequestMethod.POST, path = "/user")
	public ResponseEntity<User> addUser(@RequestParam(value = "firstName") String firstName,
								  @RequestParam(value = "lastName") String lastName) {
		User newUser = new User(firstName, lastName);
		repository.save(newUser);
		return ResponseEntity.ok(newUser);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/users")
	public List<User> getUsers() {
		return repository.findAll();
	}

	@RequestMapping(method = RequestMethod.GET, path = "/findByFirstName")
	public User getUserByFirstName(@RequestParam(value = "firstName") String firstName) {
		return repository.findByFirstName(firstName);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/findByLastName")
	public List<User> getUserByLastName(@RequestParam(value = "lastName") String lastName) {
		return repository.findByLastName(lastName);
	}

	@RequestMapping(method = RequestMethod.DELETE, path = "/{id}")
	public ResponseEntity<String> deleteUser(@PathVariable String id) {
		Optional<User> deletedUser = repository.findById(id);
		if(deletedUser.isPresent()) {
			repository.delete(deletedUser.get());
			return ResponseEntity.ok("User with id " + id + " was deleted");
		} else {
			throw new NotFoundException("User with id " + id + " cannot be found");
		}
	}

	@GetMapping("/exception/{exception_id}")
	public void getSpecificException(@PathVariable("exception_id") String pException) {
		if("409".equals(pException)) {
			throw new IllegalArgumentException();
		} else if("403".equals(pException) || "401".equals(pException)) {
			throw new AccessDeniedException("Access Denied");
		} else if("404".equals(pException)) {
			throw new NotFoundException("Not found");
		} else if("500".equals(pException)) {
			throw new NullPointerException("Null Pointer Exception");
		} else {
			throw new RuntimeException();
		}
	}

	@ExceptionHandler({ HttpClientErrorException.NotFound.class,
            ClassNotFoundException.class, NotFoundException.class})
    public ResponseEntity<Object> handleNotFoundException(Exception ex) {
        return new ResponseEntity<Object>(new NotFoundException(ex.getMessage()),
                new HttpHeaders(), HttpStatus.NOT_FOUND);
    }
}
