package com.example.springboot.config.handler;

import com.example.springboot.config.handler.exception.CustomException;
import com.example.springboot.config.handler.exception.NotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ AccessDeniedException.class, HttpClientErrorException.Unauthorized.class,
            HttpClientErrorException.Forbidden.class, ResourceAccessException.class})
    public ResponseEntity<Object> handleAccessDeniedException(Exception ex, WebRequest request) {
        return new ResponseEntity<Object>(new CustomException(ex.getMessage()), new HttpHeaders(),
                HttpStatus.FORBIDDEN);
    }

//    @ExceptionHandler({ HttpClientErrorException.NotFound.class,
//            ClassNotFoundException.class, NotFoundException.class})
//    public ResponseEntity<Object> handleNotFoundException(Exception ex, WebRequest request) {
//        return new ResponseEntity<Object>(new NotFoundException(ex.getMessage()),
//                new HttpHeaders(), HttpStatus.NOT_FOUND);
//    }

//    @ExceptionHandler
//    public ResponseEntity<Object> handleUncheckedException(Exception ex, WebRequest request) {
//        return new ResponseEntity<Object>(new RuntimeException(ex.getMessage()),
//                new HttpHeaders(), HttpStatus.BAD_REQUEST);
//    }
}
