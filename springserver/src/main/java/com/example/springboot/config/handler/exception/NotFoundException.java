package com.example.springboot.config.handler.exception;

public class NotFoundException extends CustomException {
    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, String messageId) {
        super(message, messageId);
    }
}
