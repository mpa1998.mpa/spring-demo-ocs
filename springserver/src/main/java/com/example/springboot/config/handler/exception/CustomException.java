package com.example.springboot.config.handler.exception;

import org.apache.commons.lang3.StringUtils;

public class CustomException extends RuntimeException {
    private static final long serialVersionUID = 3182248433901239456L;

    protected String messageId;

    public CustomException(String message) {
        super(message);
    }

    public CustomException(String message, String messageId) {
        super(message);
        this.messageId = messageId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public boolean hasMessageId() {
        return !StringUtils.isEmpty(messageId);
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
