package com.example.springboot.config.handler;

import com.example.springboot.config.WebSecurityConfig;
import com.example.springboot.controller.UserController;
import com.example.springboot.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
@Import(WebSecurityConfig.class)
public class WireMockForExceptionHandlerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserRepository repository;

    @MockBean
    KeycloakLogoutHandler keycloakLogoutHandler;

    @Test
    public void givenNotFound_whenGetSpecificException_thenNotFoundCode() throws Exception {
        String exceptionParam = "404";

        mvc.perform(get("/exception/{exception_id}", exceptionParam)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertEquals("Not found", result.getResolvedException().getMessage()));
    }

    @Test
    public void givenNotFound_whenGetSpecificException_thenAccessDenied() throws Exception {
        String exceptionParam = "401";

        mvc.perform(get("/exception/{exception_id}", exceptionParam)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andExpect(result -> assertEquals("Access Denied", result.getResolvedException().getMessage()));
    }
}
