import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserListComponent } from './user-list/user-list.component';
import { UserFormComponent } from './user-form/user-form.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { AccessDeniedComponent } from './access-denied/access-denied/access-denied.component';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  { path: 'users', component: UserListComponent },
  { path: 'user', component: UserFormComponent,
    canActivate: [AuthGuard],
    data: { roles: ['admin'] },
  },
  // { path: 'login', component: LoginComponent },
  // { path: '', component: LoginComponent },
  // { path: 'logout', component: LogoutComponent },
  {
    path: 'access-denied',
    component: AccessDeniedComponent,
    canActivate: [AuthGuard],
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }