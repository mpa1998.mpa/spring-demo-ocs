import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { AuthenticationService } from './service/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit {
  title: string;
  username!: string;

  constructor(public loginService:AuthenticationService,
    public keycloakService:KeycloakService) {
    this.title = 'Spring Boot - Angular Application';
    this.username = keycloakService.getUsername();
  }
  ngOnInit() {
  }

  logout() {
    this.keycloakService.logout();
  }
}
