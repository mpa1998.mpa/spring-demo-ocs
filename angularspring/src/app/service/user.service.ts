import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { User } from '../model/user';
import { Observable } from 'rxjs';

@Injectable()
export class UserService {

  private usersUrl: string;

  constructor(private http: HttpClient) {
    this.usersUrl = 'http://localhost:8081';
  }

  public findAll(): Observable<User[]> {
    // let username='admin';
    // let password='password';
    // const headers = new HttpHeaders({ Authorization: 'Basic ' + window.btoa(username + ':' + password) });
    return this.http.get<User[]>(this.usersUrl + '/users');
  }

  public save(user: User) {
    // let username='admin';
    // let password='password';
    // const headers = new HttpHeaders({ Authorization: 'Basic ' + window.btoa(username + ':' + password) });
    let body = new HttpParams();
    body = body.set('firstName', user.firstName);
    body = body.set('lastName', user.lastName);
    return this.http.post<User>(this.usersUrl + '/user', body);
  }
}
