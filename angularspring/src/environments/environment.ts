export const environment = {
    production: false,
    serverUrl: '/',
    keycloak: {
      // Url of the Identity Provider
      issuer: 'http://127.0.0.1:33449',
      // Realm
      realm: 'SpringDemoRealm',
      clientId: 'springdemo',
    },
  };